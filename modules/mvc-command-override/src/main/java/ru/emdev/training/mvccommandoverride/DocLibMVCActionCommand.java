package ru.emdev.training.mvccommandoverride;

import com.liferay.document.library.constants.DLPortletKeys;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.Constants;
import com.liferay.portal.kernel.util.ParamUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author ebaklanov
 */
@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + DLPortletKeys.DOCUMENT_LIBRARY,
                "javax.portlet.name=" + DLPortletKeys.DOCUMENT_LIBRARY_ADMIN,
                "javax.portlet.name=" + DLPortletKeys.MEDIA_GALLERY_DISPLAY,
                "mvc.command.name=/document_library/edit_folder",
                "service.ranking:Integer=100"
        },
        service = MVCActionCommand.class
)
public class DocLibMVCActionCommand extends BaseMVCActionCommand {
    private static final Log log = LogFactoryUtil.getLog(DocLibMVCActionCommand.class);

    @Reference(
            target="(component.name=com.liferay.document." +
                    "library.web.internal.portlet.action.EditFolderMVCActionCommand)"
    )
    private MVCActionCommand mvcActionCommand;

    @Override
    protected void doProcessAction(
            javax.portlet.ActionRequest actionRequest, javax.portlet.ActionResponse actionResponse)
            throws Exception {
        String cmd = ParamUtil.getString(actionRequest, Constants.CMD);

        log.info("CMD= " + cmd);

        mvcActionCommand.processAction(actionRequest, actionResponse);
    }

}