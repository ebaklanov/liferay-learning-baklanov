Этот модуль переопределяет MVCActionCommand с именем "/document_library/edit_folder" для портлетов xxx_portlet_DLPortlet, xxx_portlet_DLAdminPortlet и xxx_IGDisplayPortlet.

При срабатывании переопределяемой ActionCommand в лог выводится сообщение с типом команды.

Проверить работу можно добавив или изменив папку в "Документах и Медиа файлах" раздела "Контент".

*(xxx = com_liferay_document_library_web_portlet)