package ru.emdev.training.user.post.update.model.listener;

import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailService;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.model.User;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

@Component(
        immediate = true,
        service = ModelListener.class
)
public class UserPostUpdateModelListener extends BaseModelListener<User> {
    private static final Log log = LogFactoryUtil.getLog(UserPostUpdateModelListener.class);

    @Reference
    protected MailService mailService;

    @Override
    public void onAfterUpdate(User model) {
        try{
            MailMessage message = new MailMessage();

            message.setSubject("Security Alert: Account Settings!");
            message.setBody("Liferay has detected that your account settings have been changed");

            InternetAddress toAddress = new InternetAddress(model.getEmailAddress());
            InternetAddress fromAddress = new InternetAddress("do-not-reply@liferay.com");

            message.setTo(toAddress);
            message.setFrom(fromAddress);

            mailService.sendEmail(message);
        } catch (AddressException e) {
            log.error(e, e);
        }

        super.onAfterUpdate(model);
    }
}
