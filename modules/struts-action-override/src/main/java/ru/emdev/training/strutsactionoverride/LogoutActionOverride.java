package ru.emdev.training.strutsactionoverride;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.struts.StrutsAction;
import org.osgi.service.component.annotations.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component(
        immediate = true,
        property = {
                "path=/portal/logout"
        },
        service = StrutsAction.class
)
public class LogoutActionOverride extends BaseStrutsAction {
    private static final Log log = LogFactoryUtil.getLog(LogoutActionOverride.class);

    @Override
    public String execute(
            StrutsAction originalStrutsAction, HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse)
            throws Exception {
        log.info("Overriding Logout Struts Action");

        originalStrutsAction.execute(httpServletRequest, httpServletResponse);

        httpServletResponse.sendRedirect("https://learn.liferay.com/");

        return super.execute(originalStrutsAction, httpServletRequest, httpServletResponse);
    }
}
