Модуль при помощи Expando API добавляет поле "linkedin_profile_id" к классу User.

При старте портала модуль проверят наличие ExpandoTable для класса User, если таблицы нет, добавляет ее. Затем Модуль проверяет наличие поля "linkedin_profile_id" в таблице, если поля нет, добавляет его. 