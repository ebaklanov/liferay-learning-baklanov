package ru.emdev.training.expando;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.expando.kernel.model.ExpandoTable;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.expando.kernel.service.ExpandoTableLocalService;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.CompanyThreadLocal;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author ebaklanov
 */
@Component(
        immediate = true,
        property = {
                "key=application.startup.events"
        },
        service = LifecycleAction.class
)
public class UserExpandoStartupAction implements LifecycleAction {
    private static final Log log = LogFactoryUtil.getLog(UserExpandoStartupAction.class);

    @Reference
    private ExpandoTableLocalService expandoTableLocalService;

    @Reference
    private ExpandoColumnLocalService expandoColumnLocalService;

    @Override
    public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {
        log.info("Lifecycle Event: " + lifecycleEvent);

        long companyId = CompanyThreadLocal.getCompanyId();

        try{
            ExpandoTable expandoTable = expandoTableLocalService.fetchDefaultTable(companyId, User.class.getName());

            if (expandoTable == null) {
                expandoTable = expandoTableLocalService.addDefaultTable(companyId, User.class.getName());
            }

            ExpandoColumn expandoColumn = expandoColumnLocalService.getColumn(
                    expandoTable.getCompanyId(), "linkedin_profile_id");

            if (expandoColumn == null) {
                expandoColumnLocalService.addColumn(
                        expandoTable.getTableId(), "linkedin_profile_id", ExpandoColumnConstants.STRING);
            }
        } catch (PortalException pe) {
            log.error(pe.getMessage());
        }
    }

}