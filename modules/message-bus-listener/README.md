Этот модуль является MessageListener'ом MessageBus'a для document_library_pdf_processor'а.

Когда мы загружаем новый pdf-документ на портал через портлет Документы и Медиа Файлы, наш MessageListener пишет в логи название нового pdf-документа.