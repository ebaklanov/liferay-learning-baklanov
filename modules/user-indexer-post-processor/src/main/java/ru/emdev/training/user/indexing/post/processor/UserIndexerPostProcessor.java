package ru.emdev.training.user.indexing.post.processor;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.*;
import com.liferay.portal.kernel.search.filter.BooleanFilter;
import org.osgi.service.component.annotations.Component;

import javax.jws.soap.SOAPBinding;
import java.util.Locale;

@Component(
        immediate = true,
        property = {
                "indexer.class.name=com.liferay.portal.kernel.model.User"
        },
        service = IndexerPostProcessor.class
)
public class UserIndexerPostProcessor implements IndexerPostProcessor {
    Log log = LogFactoryUtil.getLog(UserIndexerPostProcessor.class);

    @Override
    public void postProcessContextBooleanFilter(
            BooleanFilter booleanFilter, SearchContext searchContext)
            throws Exception {

    }

    @Override
    public void postProcessDocument(Document document, Object obj) throws Exception {
        log.info("Inside of postProcessDocument");

        User user = (User)obj;

        int male = 0;

        if (user.isMale()) {
            male = 1;
        }

        document.addNumber("male", male);
    }

    @Override
    public void postProcessFullQuery(BooleanQuery fullQuery, SearchContext searchContext) throws Exception {

    }

    @Override
    public void postProcessSearchQuery(
            BooleanQuery searchQuery, BooleanFilter booleanFilter, SearchContext searchContext)
            throws Exception {
        log.info("Inside of postProcessSearchQuery");

        String keywords = searchContext.getKeywords();

        if (keywords == null) {
            return;
        }

        searchQuery.addTerm("UserId", searchContext.getKeywords());

        if (keywords.contains("boys") || keywords.contains("men")) {
            searchQuery.addTerm("male", 1);
        }
    }

    @Override
    public void postProcessSummary(Summary summary, Document document, Locale locale, String snippet) {

    }
}
