package ru.emdev.training.login.events.post;

import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailService;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;

import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserService;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * @author ebaklanov
 */
@Component(
        immediate = true,
        property = {
                "key=login.events.post"
        },
        service = LifecycleAction.class
)
public class PostLoginEventListener implements LifecycleAction {
    private static final Log log = LogFactoryUtil.getLog(PostLoginEventListener.class);

    @Reference
    protected UserService userService;

    @Reference
    protected MailService mailService;

    @Override
    public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {
        try {
            User user = userService.getCurrentUser();

            MailMessage message = new MailMessage();

            message.setSubject("Security Alert!");
            message.setBody("Liferay has detected that you have logged in. If this is not you, " +
                    "please contact your administrator");

            InternetAddress toAddress = new InternetAddress(user.getEmailAddress());
            InternetAddress fromAddress = new InternetAddress("do-not-reply@liferay.com");

            message.setTo(toAddress);
            message.setFrom(fromAddress);

            mailService.sendEmail(message);
        } catch (PortalException | AddressException e) {
            log.error(e, e);
        }
    }

}